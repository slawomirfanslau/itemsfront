<?php
declare(strict_types=1);

namespace ItemsFront\Infrastructure\Rest;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use ItemsFront\InvalidDataException;

class Rest
{
    /** @var Client */
    private $client;

    /** @var string */
    private $url;

    public function __construct(Client $client, string $url)
    {
        $this->client = $client;
        $this->url = $url;
    }

    public function post(string $url, array $params): array
    {
        $request = new Request(
            'POST',
            sprintf('%s/%s', $this->url, $url),
            ['content-type' => 'application/x-www-form-urlencoded', 'x-api-version' => 1],
            http_build_query($params)
        );

        $response = $this->client->send($request);
        if ($response->getStatusCode() != 201) {
            throw new InvalidDataException();
        }
        try {
            $data = \GuzzleHttp\json_decode($response->getBody(), true);
        } catch(\Exception $ex) {
            throw new InvalidDataException();
        }
        if (! is_array($data)) {
            throw new InvalidDataException();
        }
        return $data;
    }

    public function put(string $url, array $params): array
    {
        $request = new Request(
            'PUT',
            sprintf('%s/%s', $this->url, $url),
            ['content-type' => 'application/x-www-form-urlencoded', 'x-api-version' => 1],
            http_build_query($params)
        );

        $response = $this->client->send($request);
        if ($response->getStatusCode() != 200) {
            throw new InvalidDataException();
        }
        try {
            $data = \GuzzleHttp\json_decode($response->getBody(), true);
        } catch(\Exception $ex) {
            throw new InvalidDataException();
        }
        if (! is_array($data)) {
            throw new InvalidDataException();
        }
        return $data;
    }

    public function get(string $url, array $params = []): array
    {
        $request = new Request(
            'GET',
            sprintf('%s/%s%s', $this->url, $url, !empty($params) ? '?'.http_build_query($params) : ''),
            ['content-type' => 'text/plain', 'x-api-version' => 1]
        );

        $response = $this->client->send($request);
        if ($response->getStatusCode() != 200) {
            throw new InvalidDataException();
        }
        try {
            $data = \GuzzleHttp\json_decode($response->getBody(), true);
        } catch(\Exception $ex) {
            throw new InvalidDataException();
        }
        if (! is_array($data)) {
            throw new InvalidDataException();
        }
        return $data;
    }

    public function delete(string $url): void
    {
        $request = new Request(
            'DELETE',
            sprintf('%s/%s', $this->url, $url),
            ['content-type' => 'text/plain', 'x-api-version' => 1]
        );

        $response = $this->client->send($request);
        if ($response->getStatusCode() != 204) {
            throw new InvalidDataException();
        }
    }
}