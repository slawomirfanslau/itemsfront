<?php
declare(strict_types=1);

namespace ItemsFront\Infrastructure\Rest;

use ItemsFront\Domain\Item;
use ItemsFront\Domain\ItemRepository;

class ItemRestRepository implements ItemRepository
{
    /** @var Rest */
    private $client;

    public function __construct(Rest $client)
    {
        $this->client = $client;
    }

    public function findAll(int $minAmount = null, int $maxAmount = null): array
    {
        $params = [];
        if ($minAmount !== null) {
            $params['min_amount'] = $minAmount;
        }
        if ($maxAmount !== null) {
            $params['max_amount'] = $maxAmount;
        }

        $data = $this->client->get('items', $params);

        return array_map(function($row) {
            return new Item($row['id'], $row['name'], $row['amount']);
        }, $data['items']);
    }

    public function add(string $name, int $amount): void
    {
        $this->client->post('items', ['name' => $name, 'amount' => $amount]);
    }

    public function edit(int $id, string $name, int $amount): void
    {
        $this->client->put(sprintf('items/%d', $id), ['name' => $name, 'amount' => $amount]);
    }

    public function find(int $id): Item
    {
        $data = $this->client->get(sprintf('items/%d', $id));

        return new Item($data['item']['id'], $data['item']['name'], $data['item']['amount']);
    }

    public function remove(int $id): void
    {
        $this->client->delete(sprintf('items/%d', $id));
    }
}