<?php
declare(strict_types=1);

namespace ItemsFront\Controller;

use ItemsFront\Form\ItemType;
use ItemsFront\Infrastructure\Rest\ItemRestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ItemsController extends Controller
{
    /**
     * @Route("/index", name="items")
     * @Template("index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        return ['items' => $this->get(ItemRestRepository::class)->findAll()];
    }

    /**
     * @Route("/instock", name="in-stock-items")
     * @Template("index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function inStockAction(Request $request)
    {
        return ['items' => $this->get(ItemRestRepository::class)->findAll(1)];
    }

    /**
     * @Route("/outofstock", name="out-of-stock-items")
     * @Template("index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function outOfStockAction(Request $request)
    {
        return ['items' => $this->get(ItemRestRepository::class)->findAll(null, 0)];
    }

    /**
     * @Route("/minfive", name="min-six-items")
     * @Template("index.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function minSixAction(Request $request)
    {
        return ['items' => $this->get(ItemRestRepository::class)->findAll(6)];
    }

    /**
     * @Route("/new", name="new-item")
     * @Template("new_item.html.twig")
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function newItemAction(Request $request)
    {
        $form = $this->createForm(ItemType::class);
        $form->handleRequest($request);

        $error = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $this->get(ItemRestRepository::class)->add($data['name'], $data['amount']);

                return $this->redirectToRoute('items');
            } catch (\Exception $ex) {
                $error = true;
            }
        }

        return [
            'form' => $form->createView(),
            'error' => $error
        ];
    }

    /**
     * @Route("/edit/{id}", name="edit-item")
     * @Template("edit_item.html.twig")
     *
     * @param int $id
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function editItemAction(int $id, Request $request)
    {
        $repository = $this->get(ItemRestRepository::class);

        $form = $this->createForm(ItemType::class, $repository->find($id)->toArray());
        $form->handleRequest($request);

        $error = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $repository->edit($id, $data['name'], $data['amount']);

                return $this->redirectToRoute('items');
            } catch (\Exception $ex) {
                $error = true;
            }
        }

        return [
            'form' => $form->createView(),
            'error' => $error
        ];
    }

    /**
     * @Route("/remove/{id}", name="remove-item")
     * @Template()
     *
     * @param int $id
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function removeItemAction(int $id, Request $request)
    {
        $this->get(ItemRestRepository::class)->remove($id);

        return $this->redirectToRoute('items');
    }
}