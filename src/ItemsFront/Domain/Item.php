<?php
declare(strict_types=1);

namespace ItemsFront\Domain;

use Assert\Assertion;

class Item
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var int */
    private $amount;

    public function __construct(int $id, string $name, int $amount)
    {
        Assertion::notEmpty($name);
        Assertion::greaterOrEqualThan($amount, 0);

        $this->id = $id;
        $this->name = $name;
        $this->amount = $amount;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'amount' => $this->amount
        ];
    }
}