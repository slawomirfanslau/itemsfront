<?php
declare(strict_types=1);

namespace ItemsFront\Domain;

interface ItemRepository
{
    public function findAll(int $minAmount = null, int $maxAmount = null): array;

    public function add(string $name, int $amount): void;

    public function find(int $id): Item;

    public function remove(int $id): void;
}